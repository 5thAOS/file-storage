package com.example.rany.filestoragedemo;

import android.Manifest;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileStorageDemos extends AppCompatActivity {

    private Button readInternal, writeInternal, readExternal, writeExternal;
    private EditText input;
    private TextView output;

    public static final String FILE_NAME = "file";
    public static final int PERMISSION_WRITE = 1;
    public static final int PERMISSION_READ = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        writeInternal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeToInternal(input.getText().toString());
            }
        });

        readInternal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                readFromInternal();
            }
        });

        writeExternal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(ContextCompat.checkSelfPermission(FileStorageDemos.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                        getPackageManager().PERMISSION_GRANTED){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            1);
                    }
                }
                else
                    writeToExternal(input.getText().toString());
            }
        });

        readExternal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ContextCompat.checkSelfPermission(FileStorageDemos.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE) !=
                        getPackageManager().PERMISSION_GRANTED){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                PERMISSION_READ);
                    }
                }
                else
                    readFromExternal();
            }
        });

    }

    private void readFromExternal() {

        File path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM
        );
        StringBuilder builder = new StringBuilder();
        FileInputStream read = null;
        try {
            read = new FileInputStream(new File(path, FILE_NAME));
            int c;
            while ((c = read.read()) != -1){
                char c1 = (char) c;
                builder.append(c1);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            output.setText(builder.toString());
            try {
                read.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void writeToExternal(String value) {

        File path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM
        );
        FileOutputStream write = null;
        try {
          write = new FileOutputStream(new File(path, FILE_NAME));
            write.write(value.getBytes());
            write.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(write != null){
                try {
                    write.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void readFromInternal() {

        StringBuilder builder = new StringBuilder();
        FileInputStream read = null;

        try {
            read = openFileInput(FILE_NAME);
            int c;
            while ((c = read.read()) != -1){
                char result = (char) c;
                builder.append(result);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            output.setText(builder.toString());
            try {
                read.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void writeToInternal(String value) {
        FileOutputStream write = null;
        try {
            write = openFileOutput(FILE_NAME, MODE_PRIVATE);
            write.write(value.getBytes());
            write.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(write != null){
                try {
                    write.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void initView() {
        readInternal = findViewById(R.id.btnReadInternal);
        writeInternal = findViewById(R.id.btnWriteInternal);
        input = findViewById(R.id.tvInputText);
        output = findViewById(R.id.tvOutPut);
        readExternal = findViewById(R.id.btnReadExternal);
        writeExternal = findViewById(R.id.btnWriteExternal);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 1){
            if(grantResults[0] == getPackageManager().PERMISSION_GRANTED){
                writeToExternal(input.getText().toString());
            }
        }
        if(requestCode == PERMISSION_READ){
            if(grantResults[0] == getPackageManager().PERMISSION_GRANTED){
                readFromExternal();
            }
        }
    }
}
